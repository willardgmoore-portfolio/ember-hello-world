Following
https://guides.emberjs.com/v2.9.0/getting-started/quick-start/

Error installing ember
----
$ npm install -g ember-cli@2.9
npm ERR! tar.unpack untar error /Users/willardmoore/.npm/ember-cli/2.9.1/package.tgz
npm ERR! Darwin 16.0.0
npm ERR! argv "/usr/local/bin/node" "/usr/local/bin/npm" "install" "-g" "ember-cli@2.9"
npm ERR! node v4.2.3
npm ERR! npm  v2.14.7
npm ERR! path /usr/local/lib/node_modules/ember-cli
npm ERR! code EACCES
npm ERR! errno -13
npm ERR! syscall mkdir

npm ERR! Error: EACCES: permission denied, mkdir '/usr/local/lib/node_modules/ember-cli'
npm ERR!     at Error (native)
npm ERR!  { [Error: EACCES: permission denied, mkdir '/usr/local/lib/node_modules/ember-cli']
npm ERR!   errno: -13,
npm ERR!   code: 'EACCES',
----

----
Solved by changing npm permissions
https://docs.npmjs.com/getting-started/fixing-npm-permissions
--
sudo chown -R $(whoami) $(npm config get prefix)/{lib/node_modules,bin,share}
----

TODO:
What was this about after creating a new ember project?
"Could not start watchman; falling back to NodeWatcher for file system events."

TODO:
Learn about the newest features of JavaScript
https://ponyfoo.com/articles/es6


